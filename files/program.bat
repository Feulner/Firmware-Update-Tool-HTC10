@echo off
cd "%~dp0"
set "anotherfl=disable"
cls
echo #############################################
echo #     This is krassermensch's tool for      #
echo #     flashing HTC 10 firmware without      #
echo #   the need of setting the device S-Off    #
echo #     That means you don't need to pay      #
echo #     money for a tool that sets your       #
echo #    device S-Off just because you want     #
echo #         to update your firmware.          #
echo #############################################
timeout /t 15 >NUL
cls
echo Do you agree that I (krassermensch) am not
echo responsible for any kind of damage that may
echo occur to your device? Do you also agree that
echo your data partition may be wiped after the
echo process?
:agreement
set /p agree="(y)Yes, I agree -- (n)No, I don't agree  "
if %agree%==n (
goto:deny
) else if %agree%==y (
echo Let's proceed...
timeout /t 3 >NUL
cls
) else (
echo Invalid choice
timeout /t 3 >NUL
goto:agreement
)

echo Do you want to install the USB drivers for your HTC 10?
echo If you have already installed them select No.
echo If you haven't select Yes.
:agree
set /p agree="(y)Yes, I want -- (n)No, I don't want  "
if %agree%==n (
goto:allright
) else if %agree%==y (
echo Let's install it...
call driver.exe
echo -- Press Enter to proceed --
pause >NUL
cls
goto:allright
) else (
echo Invalid choice
timeout /t 3 >NUL
goto:agree
)

:allright
cls
echo Do you know how to enable USB-Debugging?
set /p usbdebugging="(y)Yes, I know how to do the procedure / (n)No, please tell me!  "
if %usbdebugging%==y (
cls
echo Connect your HTC 10 via USB cable and
echo make sure USB-Debugging is enabled.
echo Remember to accept any USB-Debugging
echo request on your phone.
timeout /t 8 >NUL
goto:connect
) else if %usbdebugging%==n (
cls
echo Connect your HTC 10 now via USB cable...
echo -- Press Enter to go on --
pause >NUL
echo Turn on your device now
echo -- Press Enter to go on --
pause >NUL
echo After that go to Settings app
echo -- Press Enter to go on --
pause >NUL
echo Go to About Phone
echo -- Press Enter to go on --
pause >NUL
echo Go to Software-Information
echo -- Press Enter to go on --
pause >NUL
echo Go to More
echo -- Press Enter to go on --
pause >NUL
echo Press Build-Number 7 times
echo -- Press Enter to go on --
pause >NUL
echo Return to the main screen of the settings app
echo -- Press Enter to go on --
pause >NUL
echo Go to Developer Settings
echo -- Press Enter to go on --
pause >NUL
echo Look for USB-Debugging and enable it
echo -- Press Enter to go on --
pause >NUL
echo Accept any USB-Debugging request on your device
echo -- Press Enter to go on --
pause >NUL
goto:connect
) else (
echo Invalid choice
timeout /t 3 >NUL
goto:allright
)

:connect
cls
echo Did you set up everything correctly?
set /p ready="(y)Yes, I connected my device and I'm ready to go!  "
if %ready%==y (
cls
echo Checking connection now...
echo Remember to accept any USB-Debugging
echo request on your phone.
timeout /t 3 >NUL
) else (
echo Invalid choice
timeout /t 3 >NUL
goto:connect
)
call adb devices -l | find "device product:" >NUL
if errorlevel 1 (
cls
echo Your device wasn't detected by your computer
echo Let's retry the setup!
timeout /t 5 >NUL
goto:allright
) else (
cls
echo Everything seems to work...
echo -- Press Enter to go on --
pause >NUL
)

:twrpselection
echo Have you already installed TeamWinRecoveryProject (TWRP)?
echo Do you also have an unlocked bootloader?
echo It is required to perform a backup.
echo If you don't know you probably haven't.
set /p twrp="(y)Yes, I've installed it and my bootloader is unlocked / (n)No, I haven't  "
if %twrp%==y (
cls
echo Your selection: TWRP is installed
echo -- Press Enter to go on --
pause >NUL
) else if %twrp%==n (
cls
echo Your selection: TWRP is not installed
echo Run my tool again when it's installed on your device.
echo Here's a guide about how to install TWRP on your HTC 10:
start "" http://forum.xda-developers.com/htc-10/how-to/guide-root-optionally-s-off-radio-t3373025
timeout /t 7 >NUL
exit
) else (
echo Invalid choice
timeout /t 3 >NUL
cls
goto:twrpselection
)

:initialstart
cls
echo Your device will reboot to Recovery Mode now.
echo If you're asked to allow an adb action on
echo your phone before it reboots just accept it.
echo Don't press anything when you're in Recovery Mode!
echo -- Press Enter to go on --
pause >NUL
cls
call adb reboot recovery >NUL
call adb kill-server >NUL
timeout /t 25 /nobreak >NUL

:recoverymode
call adb devices -l | find "recovery" >NUL
if errorlevel 1 (
echo Your device was not detected by your computer.
echo Make sure your HTC 10 USB drivers are also installed
echo for the device being in recovery. If you set everything
echo up correctly press Enter to retry.
pause >NUL
cls
goto:recoverymode
) else (
echo Your device was detected...
)

echo Backing up your partitions now...
call adb shell dd if=/dev/block/platform/soc/7464900.sdhci/by-name/boot of=/sdcard/boot.img >NUL
call adb pull /sdcard/boot.img backup\boot.img >NUL 2>&1
call adb shell rm -f /sdcard/boot.img >NUL
call adb shell dd if=/dev/block/platform/soc/7464900.sdhci/by-name/recovery of=/sdcard/recovery.img >NUL
call adb pull /sdcard/recovery.img backup\recovery.img >NUL 2>&1
call adb shell rm -f /sdcard/recovery.img >NUL
call adb reboot download >NUL
timeout /t 20 /nobreak >NUL
call htc_fastboot getvar all >cache 2>&1
for /F "skip=9 delims=" %%i in (cache) do set "cmd=%%i"&goto:nextline
:nextline
set "cmd=%cmd:~18%"

cls
echo -----------------------------------------------------------------------
echo Your device's CID is %cmd%
echo -----------------------------------------------------------------------
if %cmd%==HTC__001 (
set "device=one"
goto:one
) else if %cmd%==HTC__034 (
set "device=one"
goto:one
) else if %cmd%==HTC__A07 (
set "device=one"
goto:one
) else if %cmd%==HTC__J15 (
set "device=one"
goto:one
) else if %cmd%==HTC__M27 (
set "device=one"
goto:one
) else if %cmd%==HTC__016 (
set "device=one"
goto:one
) else if %cmd%==HTC__002 (
set "device=one"
goto:one
) else if %cmd%==HTC__621 (
set "device=two"
goto:two
) else if %cmd%==HTC__039 (
set "device=three"
goto:three
) else if %cmd%==OPTUS001 (
set "device=three"
goto:three
) else if %cmd%==VODAP021 (
set "device=three"
goto:three
) else if %cmd%==TELNZ001 (
set "device=three"
goto:three
) else if %cmd%==TELST001 (
set "device=four"
goto:four
) else if %cmd%==T-MOB101 (
set "device=five"
goto:five
) else if %cmd%==BS_US001 (
set "device=six"
goto:six
) else if %cmd%==BS_US002 (
set "device=six"
goto:six
) else if %cmd%==HTC__332 (
set "device=seven"
goto:seven
) else if %cmd%==HTC__060 (
set "device=eight"
goto:eight
) else if %cmd%==HTC__059 (
set "device=eight"
goto:eight
) else if %cmd%==HTC__058 (
set "device=eight"
goto:eight
) else if %cmd%==O2___001 (
set "device=nine"
goto:nine
) else if %cmd%==O2___102 (
set "device=nine"
goto:nine
) else if %cmd%==VODAP001 (
set "device=ten"
goto:ten
) else if %cmd%==VODAP102 (
set "device=ten"
goto:ten
) else (
echo It seems like your HTC 10 model isn't supported by my tool...
goto:exit
)

:one
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline2
:nextline2
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==2.41.401.3 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.95.401.4 (
:onea
set "firmware=2.41.401.3"
set "seter=ready"
goto:start
) else if %firm%==1.95.401.3 (
:oneb
set "firmware=1.95.401.4"
set "seter=onea"
goto:start
) else if %firm%==1.90.401.5 (
:onec
set "firmware=1.95.401.3"
set "seter=oneb"
goto:start
) else if %firm%==1.80.401.3 (
:oned
set "firmware=1.90.401.5"
set "seter=onec"
goto:start
) else if %firm%==1.80.401.1 (
:onee
set "firmware=1.80.401.3"
set "seter=oned"
goto:start
) else if %firm%==1.30.401.1 (
:onef
set "firmware=1.80.401.1"
set "seter=onee"
goto:start
) else if %firm%==1.21.401.4 (
set "firmware=1.30.401.1"
set "seter=onef"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:two
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline3
:nextline3
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==1.96.709.5 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.92.709.1 (
:twoa
set "firmware=1.96.709.5"
set "seter=ready"
goto:start
) else if %firm%==1.80.709.1 (
:twob
set "firmware=1.92.709.1"
set "seter=twoa"
goto:start
) else if %firm%==1.55.709.5 (
:twoc
set "firmware=1.80.709.1"
set "seter=twob"
goto:start
) else if %firm%==1.30.709.1 (
:twod
set "firmware=1.55.709.5"
set "seter=twoc"
goto:start
) else if %firm%==1.21.709.2 (
set "firmware=1.30.709.1"
set "seter=twod"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:three
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline4
:nextline4
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==1.80.710.1 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.53.710.6 (
:threea
set "firmware=1.80.710.1"
set "seter=ready"
goto:start
) else if %firm%==1.21.710.10 (
set "firmware=1.53.710.6"
set "seter=threea"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:four
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline5
:nextline5
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==1.53.841.6 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.21.841.8 (
set "firmware=1.53.841.6"
set "seter=ready"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:five
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline6
:nextline6
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==1.95.111.4 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.95.111.3 (
:fivea
set "firmware=1.95.111.4"
set "seter=ready"
goto:start
) else if %firm%==1.90.111.5 (
:fiveb
set "firmware=1.95.111.3"
set "seter=fivea"
goto:start
) else if %firm%==1.80.111.1 (
:fivec
set "firmware=1.90.111.5"
set "seter=fiveb"
goto:start
) else if %firm%==1.53.111.7 (
:fived
set "firmware=1.80.111.1"
set "seter=fivec"
goto:start
) else if %firm%==1.21.111.4 (
set "firmware=1.53.111.7"
set "seter=fived"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:six
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline7
:nextline7
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==2.41.617.3 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==2.38.617.6 (
:sixa
set "firmware=2.41.617.3"
set "seter=ready"
goto:start
) else if %firm%==2.28.617.8 (
:sixb
set "firmware=2.38.617.6"
set "seter=sixa"
goto:start
) else if %firm%==1.96.617.20 (
:sixc
set "firmware=2.28.617.8"
set "seter=sixb"
goto:start
) else if %firm%==1.96.617.2 (
:sixd
set "firmware=1.96.617.20"
set "seter=sixc"
goto:start
) else if %firm%==1.91.617.1 (
:sixe
set "firmware=1.96.617.2"
set "seter=sixd"
goto:start
) else if %firm%==1.80.617.1 (
:sixf
set "firmware=1.91.617.1"
set "seter=sixe"
goto:start
) else if %firm%==1.53.617.5 (
:sixg
set "firmware=1.80.617.1"
set "seter=sixf"
goto:start
) else if %firm%==1.21.617.3 (
set "firmware=1.53.617.5"
set "seter=sixg"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:seven
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline8
:nextline8
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==2.24.600.1 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.90.600.1 (
:sevena
set "firmware=2.24.600.1"
set "seter=ready"
goto:start
) else if %firm%==1.53.600.31 (
:sevenb
set "firmware=1.90.600.1"
set "seter=sevena"
goto:start
) else if %firm%==1.02.600.3 (
set "firmware=1.53.600.31"
set "seter=sevenb"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:eight
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline9
:nextline9
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==1.80.400.1 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.29.400.3 (
set "firmware=1.80.400.1"
set "seter=ready"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:nine
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline10
:nextline10
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==1.53.206.6 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.21.206.4 (
set "firmware=1.53.206.6"
set "seter=ready"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:ten
cls
for /F "skip=4 delims=" %%i in (cache) do set "firm=%%i"&goto:nextline11
:nextline11
set "firm=%firm:~27%"
echo ---------------------------------------------
echo Your current firmware version is %firm%
echo ---------------------------------------------
if %firm%==1.95.161.3 (
echo You already have the latest firmware package
echo for your device supported by my tool.
echo Rebooting your device now...
goto:exit
) else if %firm%==1.90.161.4 (
:tena
set "firmware=1.95.161.3"
set "seter=ready"
goto:start
) else if %firm%==1.80.161.1 (
:tenb
set "firmware=1.90.161.4"
set "seter=tena"
goto:start
) else if %firm%==1.53.161.7 (
:tenc
set "firmware=1.80.161.1"
set "seter=tenb"
goto:start
) else if %firm%==1.21.600.4 (
set "firmware=1.53.161.7"
set "seter=tenc"
goto:start
) else (
echo Your device is runing a firmware version that's
echo unknown to my tool.
goto:exit
)

:start
cls
if %anotherfl%==enable (
echo Flashing another firmware now...
goto:anotherflash
) else (
goto:goon
)
:goon
echo Rebooting and downloading firmware now...
timeout /t 5 /nobreak >NUL
call htc_fastboot oem rebootRUU >NUL 2>&1
timeout /t 20 /nobreak >NUL
:anotherflash
call wget -O %device%\%firmware%.zip -q --show-progress https://gitlab.com/Feulner/Firmware-Update-Tool-HTC10/raw/master/files/%device%/%firmware%.zip
if not exist %device%\%firmware%.zip goto:downloaderror
cls
echo Flashing firmware now...
call htc_fastboot flash zip %device%\%firmware%.zip >NUL 2>&1
timeout /t 30 /nobreak >NUL
call htc_fastboot flash zip %device%\%firmware%.zip >NUL 2>&1
timeout /t 50 /nobreak >NUL
set "anotherfl=enable"
if %seter%==onea (
echo Another firmware also has to be flashed.
echo Just wait...
goto:onea
) else if %seter%==oneb (
echo Another firmware also has to be flashed.
echo Just wait...
goto:oneb
) else if %seter%==onec (
echo Another firmware also has to be flashed.
echo Just wait...
goto:onec
) else if %seter%==oned (
echo Another firmware also has to be flashed.
echo Just wait...
goto:oned
) else if %seter%=onee (
echo Another firmware also has to be flashed.
echo Just wait...
goto:onee
) else if %seter%=onef (
echo Another firmware also has to be flashed.
echo Just wait...
goto:onef
) else if %seter%==twoa (
echo Another firmware also has to be flashed.
echo Just wait...
goto:twoa
) else if %seter%==twob (
echo Another firmware also has to be flashed.
echo Just wait...
goto:twob
) else if %seter%==twoc (
echo Another firmware also has to be flashed.
echo Just wait...
goto:twoc
) else if %seter%==twod (
echo Another firmware also has to be flashed.
echo Just wait...
goto:twod
) else if %seter%==threea (
echo Another firmware also has to be flashed.
echo Just wait...
goto:threea
) else if %seter%==fivea (
echo Another firmware also has to be flashed.
echo Just wait...
goto:fivea
) else if %seter%==fiveb (
echo Another firmware also has to be flashed.
echo Just wait...
goto:fiveb
) else if %seter%==fivec (
echo Another firmware also has to be flashed.
echo Just wait...
goto:fivec
) else if %seter%==fived (
echo Another firmware also has to be flashed.
echo Just wait...
goto:fived
) else if %seter%==sixa (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sixa
) else if %seter%==sixb (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sixb
) else if %seter%==sixc (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sixc
) else if %seter%==sixd (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sixd
) else if %seter%==sixe (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sixe
) else if %seter%==sixf (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sixf
) else if %seter%==sixg (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sixg
) else if %seter%==sevena (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sevena
) else if %seter%==sevenb (
echo Another firmware also has to be flashed.
echo Just wait...
goto:sevenb
) else if %seter%==tena (
echo Another firmware also has to be flashed.
echo Just wait...
goto:tena
) else if %seter%==tenb (
echo Another firmware also has to be flashed.
echo Just wait...
goto:tenb
) else if %seter%==tenc (
echo Another firmware also has to be flashed.
echo Just wait...
goto:tenc
) else (
goto:end
)

:end
cls
echo Restoring your boot and recovery finally...
call htc_fastboot flash boot backup\boot.img >NUL 2>&1
timeout /t 5 /nobreak >NUL
call htc_fastboot flash recovery backup\recovery.img >NUL 2>&1
timeout /t 5 /nobreak >NUL
call htc_fastboot reboot >NUL
echo Your device is rebooting.
echo The latest firmware is flashed now.
echo Like me (krassermensch) at xda.
echo Consider sending me some money if you
echo like my work.
echo Thank you really much! ;)
echo The program will exit now...
if exist cache1 del /F cache1
timeout /t 1 /nobreak >NUL
if exist cache del /F cache
timeout /t 1 /nobreak >NUL
if exist twrp.img del /F twrp.img
timeout /t 15 >NUL
start "" https://www.paypal.me/krassermensch
exit

:downloaderror
echo Your firmware download seems to have failed
goto:exit

:deny
echo Like me (krassermensch) at xda.
echo Consider sending me some money if you
echo like my work.
echo Thank you really much! ;)
echo The program will exit now...
if exist cache del /F cache
timeout /t 1 /nobreak >NUL
if exist twrp.img del /F twrp.img
timeout /t 10 >NUL
start "" https://www.paypal.me/krassermensch
exit


:exit
call htc_fastboot reboot >NUL 2>&1
echo Like me (krassermensch) at xda.
echo Consider sending me some money if you
echo like my work.
echo Thank you really much! ;)
echo The program will exit now...
if exist cache del /F cache
timeout /t 1 /nobreak >NUL
if exist twrp.img del /F twrp.img
timeout /t 10 >NUL
start "" https://www.paypal.me/krassermensch
exit
