Krassermensch's Firmware Update Tool
==========================================


My situation:
---------------
I'm too young to buy the Sunshine App to set my device S-Off but I want to update my device's firmware every time a firmware update's available. Since I compile ROMs and cannot do that via OTA updates the only acceptable way for me is to install signed firmwares in a locked bootloader. After doing that manually I thought I should write a tool that installs the right firmware for your device automatically. As I'm already running the latest firmware on my device after doing the procedure manually I cannot test if the flash part works correctly. Anyway, I achieved to read out the CID as well as the current firmware version and my tool reports that I'm running the latest firmware version I integrated for my device what's correct. So this part works well.


How the tool works:
---------------------
The tool asks you if you've already installed the HTC USB drivers on your computer. If you haven't you can install it easily using my tool. After that the tool explains you how to set up your phone to be detected by the computer. Then it checks which device you have and checks which firmware's currently installed on it. Right after that the tool installs all the firmwares newer than the one that's currently installed on your phone one after another until the latest firmware my tool is shiped with runs on your device. The procedure doesn't wipe your data since signed firmware-updates are installed.


Requirements:
-----------------
- An HTC 10
- A computer running windows
- An internet connection


Who is this tool for?
-----------------------
All the people who can't or don't want to set their device S-Off while running a non-stock ROM.


How to use the source:
--------------------------
Download the FlashFirmware file to your local storage. Then create a new folder called files in the directory you downloaded the FlashFirmware file to. Then download the wget file from the files folder in my repo to your new created files folder. Finally just run the FlashFirmware file ;)


My tool uses the tools:
--------------------------
- GNU Wget
- HTC USB drivers
- HTC fastboot
- Android debugging bridge (ADB)

Thank you for providing your software!