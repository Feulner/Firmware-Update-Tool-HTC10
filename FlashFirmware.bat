@echo off
cd "%~dp0\files"
echo Checking dependencies and auto-updating them...
if exist wget.exe (
if not exist AdbWinApi.dll call wget -q https://gitlab.com/Feulner/Firmware-Update-Tool-HTC10/raw/master/files/AdbWinApi.dll -O AdbWinApi.dll
if not exist AdbWinUsbApi.dll call wget -q https://gitlab.com/Feulner/Firmware-Update-Tool-HTC10/raw/master/files/AdbWinUsbApi.dll -O AdbWinUsbApi.dll
if not exist adb.exe call wget -q https://gitlab.com/Feulner/Firmware-Update-Tool-HTC10/raw/master/files/adb.exe -O adb.exe
if not exist driver.exe call wget -q https://gitlab.com/Feulner/Firmware-Update-Tool-HTC10/raw/master/files/driver.exe -O driver.exe
if not exist htc_fastboot.exe call wget -q https://gitlab.com/Feulner/Firmware-Update-Tool-HTC10/raw/master/files/htc_fastboot.exe -O htc_fastboot.exe
call wget -q https://gitlab.com/Feulner/Firmware-Update-Tool-HTC10/raw/master/files/program.bat -O program.bat
if not exist one mkdir one
if not exist two mkdir two
if not exist three mkdir three
if not exist four mkdir four
if not exist five mkdir five
if not exist six mkdir six
if not exist seven mkdir seven
if not exist eight mkdir eight
if not exist nine mkdir nine
if not exist ten mkdir ten
call program.bat
exit
) else (
echo Wget is missing
echo Visit my xda thread and check if you set up my tool correctly
echo Exiting now...
timeout /t 7 >NUL
exit
)
